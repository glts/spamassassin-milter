# SpamAssassin Milter changelog

All notable user-facing changes are listed in this changelog.

This project follows Rust-flavoured [semantic versioning], with changes to the
minimum supported Rust version being considered breaking changes.

[semantic versioning]: https://doc.rust-lang.org/cargo/reference/semver.html

## 0.5.0 (2024-03-13)

With this release, development has moved from GitLab to the [Codeberg] platform.
Other than that, this is a maintenance release with no notable functional
changes.

The minimum supported Rust version is now 1.65.0.

[Codeberg]: https://codeberg.org

### Changed

* The minimum supported Rust version has been raised to 1.65.0.

## 0.4.0 (2023-01-29)

The minimum supported Rust version is now 1.61.0.

### Changed

* The minimum supported Rust version has been raised to 1.61.0.

### Fixed

* The program no longer panics when it cannot write log messages to standard
  error.

## 0.3.2 (2022-08-31)

### Changed

* The command-line user interface has been reimplemented using a lighter-weight,
  no-dependencies approach.

  While there are some differences in appearance and error reporting, there are
  no functional changes in using the program. Make sure that you are not relying
  on undocumented (and therefore unsupported) behaviour of the old CLI.

## 0.3.1 (2022-03-14)

### Fixed

* The milter’s connection timeout duration has been increased substantially. The
  previously used duration of five minutes turned out to be too short for some
  slow SMTP conversations.

## 0.3.0 (2022-03-08)

In this release, the milter implementation has been replaced with the new
[indymilter] library. With this change, there is no longer a dependency on the
libmilter C library. SpamAssassin Milter is now a pure Rust application.

The minimum supported Rust version is now 1.56.1.

### Changed

* The minimum supported Rust version is now 1.56.1 (using Rust edition 2021).
* The syntax of the mandatory `SOCKET` argument has changed:
  - Use <code>inet:<em>host</em>:<em>port</em></code> for a TCP socket.
  - Use <code>unix:<em>path</em></code> for a UNIX domain socket.
* The command-line help information has changed its appearance slightly with the
  update of the underlying [clap] CLI library.
* The changelog is now maintained in a more structured format, similar to
  https://keepachangelog.com.

[indymilter]: https://crates.io/crates/indymilter
[clap]: https://crates.io/crates/clap

## 0.2.1 (2021-12-23)

* Various cosmetic improvements in code and tests, and updates to documentation.
* Update dependencies.

## 0.2.0 (2021-08-26)

* Bump minimum supported Rust version to 1.46.0.
* (defaults change) Invoke `spamc` using the absolute path `/usr/bin/spamc`
  (instead of any executable named `spamc` in the search path). To customise
  this, set the environment variable `SPAMASSASSIN_MILTER_SPAMC` to the desired
  path when building the application.
* Revise header rewriting logic. Handling and placement of `X-Spam-` headers now
  more accurately mirrors that applied by SpamAssassin.
* Include authentication status in information passed on to SpamAssassin.
* Update dependencies.

## 0.1.6 (2021-05-17)

* Improve processing of incoming `X-Spam-Flag` headers. Previously, in rare
  circumstances a message flagged as spam would not be rejected as requested.
  Reported by Petar Bogdanovic.
* Update dependencies.

## 0.1.5 (2021-03-16)

* Read output from `spamc` in a separate thread in order to avoid blocking when
  processing large messages in certain configurations.
* Document requirement to keep `--max-message-size` setting in sync with
  `spamc`’s `--max-size` setting.
* Remove overly strict validation of command-line options.
* Properly specify minimal dependency versions in `Cargo.toml`.
* Document minimum supported Rust version 1.42.0.

## 0.1.4 (2020-10-18)

* Correct a typo in log messages.
* Isolate integration tests from any existing `spamc` configuration present on
  the host.
* Various à la mode style improvements in code and project metadata.

## 0.1.3 (2020-07-04)

* Add `--reply-code`, `--reply-status-code`, and `--reply-text` options to allow
  customising the SMTP reply when rejecting spam.
* Log a warning and do not truncate the message body when `--max-message-size`
  is misconfigured (must be ≥ `spamc` max size as documented).
* Update dependencies in `Cargo.lock`.

## 0.1.2 (2020-06-07)

* Bump milter dependency to version 0.2.1.
* Remove existing UNIX domain socket at target path during startup.
* Derive `Eq` and `PartialEq` for configuration structs.

## 0.1.1 (2020-04-13)

* Use `Write::write_all` instead of `Write::write` in `spamc` client, in order
  to ensure buffers are written in their entirety.
* Do not include `.gitignore` file in published crate.

## 0.1.0 (2020-02-23)

Initial release.
