# SpamAssassin Milter

The SpamAssassin Milter project has moved to the [Codeberg] platform:

<https://codeberg.org/glts/spamassassin-milter>

Please update your links.

[Codeberg]: https://codeberg.org
