mod common;

pub use common::*;

use indymilter_test::*;
use std::net::Ipv4Addr;

#[tokio::test]
async fn loopback_connection() {
    let milter = SpamAssassinMilter::spawn(LOCALHOST, Default::default())
        .await
        .unwrap();

    // 1) A connection from the loopback IP address is accepted.

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("localhost", Ipv4Addr::LOCALHOST).await.unwrap();
    assert_eq!(status, Status::Accept);

    conn.close().await.unwrap();

    // 2) A connection from an ‘unknown’ IP address (for example, from a UNIX
    // domain socket) is also accepted.

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("localhost", SocketInfo::Unknown).await.unwrap();
    assert_eq!(status, Status::Accept);

    conn.close().await.unwrap();

    milter.shutdown().await.unwrap();
}
