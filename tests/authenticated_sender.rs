mod common;

pub use common::*;

use indymilter_test::*;

/// An authenticated sender is accepted, the message is not processed.
#[tokio::test]
async fn authenticated_sender() {
    let milter = SpamAssassinMilter::spawn(LOCALHOST, Default::default())
        .await
        .unwrap();

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("client.gluet.ch", [123, 123, 123, 123]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.helo("mail.gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    // `{auth_authen}` holds the SASL login name, if any.
    conn.macros(MacroStage::Mail, [("{auth_authen}", "from@gluet.ch")])
        .await
        .unwrap();

    let status = conn.mail(["<from@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Accept);

    conn.close().await.unwrap();

    milter.shutdown().await.unwrap();
}
