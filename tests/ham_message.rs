mod common;

pub use common::*;

use indymilter_test::*;
use spamassassin_milter::*;

/// ‘Happy path’ processing of an ordinary ham (not spam) message.
#[tokio::test]
async fn ham_message() {
    let config = configure_spamc(Config::builder())
        .spamc_args([format!("--port={SPAMD_PORT}")])
        .build();

    let server = spawn_mock_spamd_server(SPAMD_PORT, |ham| {
        let mut ham = ham
            .replacen("X-Spam-Checker-Version: BogusChecker 1.0.0\r\n", "", 1)
            .replacen("X-Spam-Report: Bogus report\r\n", "", 1);

        // Prepend replaced and newly added SpamAssassin headers.
        ham.insert_str(
            0,
            "X-Spam-Checker-Version: MyChecker 1.0.0\r\n\
             X-Spam-Custom: Custom-Value\r\n",
        );

        Ok(ham)
    })
    .await
    .unwrap();

    let milter = SpamAssassinMilter::spawn(LOCALHOST, config).await.unwrap();

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("client.gluet.ch", [123, 123, 123, 123]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.helo("mail.gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.mail(["<from@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.rcpt(["<to@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    conn.macros(
        MacroStage::Data,
        [
            ("i", "1234567ABC"),
            ("j", "localhost"),
            ("_", "client.gluet.ch [123.123.123.123]"),
            ("{tls_version}", "TLSv1.2"),
            ("v", "Postfix 3.3.0"),
        ],
    )
    .await
    .unwrap();

    let status = conn.data().await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("From", "from@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("To", "to@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Subject", "Test message").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Message-ID", rand_msg_id()).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Date", current_date()).await.unwrap();
    assert_eq!(status, Status::Continue);

    // Incoming foreign SpamAssassin headers, to be replaced or deleted.
    let status = conn.header("X-Spam-Checker-Version", "BogusChecker 1.0.0").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("X-Spam-Report", "Bogus report").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.eoh().await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.body(&b"Hello, we would like to invite you to ..."[..]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let (actions, status) = conn.eom().await.unwrap();
    assert_eq!(status, Status::Continue);

    assert!(actions.has_insert_header(0, "X-Spam-Custom", " Custom-Value"));
    assert!(actions.has_delete_header("X-Spam-Checker-Version", any()));
    assert!(actions.has_insert_header(0, "X-Spam-Checker-Version", " MyChecker 1.0.0"));
    assert!(actions.has_delete_header("X-Spam-Report", any()));

    conn.close().await.unwrap();

    milter.shutdown().await.unwrap();

    server.await.unwrap().unwrap();
}
