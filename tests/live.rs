mod common;

pub use common::*;

use indymilter_test::*;
use spamassassin_milter::*;

/// Runs a ‘live’ test against a real SpamAssassin server instance. This test is
/// run on demand, as SpamAssassin will actually analyse the input, and do DNS
/// queries etc.
#[tokio::test]
#[ignore = "runs live test against SpamAssassin server"]
async fn live() {
    // When no port is specified, `spamc` will try to connect to the default
    // `spamd` port 783 (see also `/etc/services`).
    let config = configure_spamc(Config::builder()).build();

    let milter = SpamAssassinMilter::spawn(LOCALHOST, config).await.unwrap();

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("client.gluet.ch", [123, 123, 123, 123]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.helo("mail.gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.mail(["<from@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.rcpt(["<to@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    conn.macros(
        MacroStage::Data,
        [
            ("i", "1234567ABC"),
            ("j", "localhost"),
            ("_", "client.gluet.ch [123.123.123.123]"),
            ("{tls_version}", "TLSv1.2"),
            ("v", "Postfix 3.3.0"),
        ],
    )
    .await
    .unwrap();

    let status = conn.data().await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("From", "from@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("To", "to@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Subject", "Test message").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Message-ID", rand_msg_id()).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Date", current_date()).await.unwrap();
    assert_eq!(status, Status::Continue);

    // Add headers here to experiment.

    let status = conn.eoh().await.unwrap();
    assert_eq!(status, Status::Continue);

    // This is the magic GTUBE value, which makes this message certain spam.
    // See https://spamassassin.apache.org/gtube/.
    let status = conn.body(
        &b"XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X"[..],
    )
    .await
    .unwrap();
    assert_eq!(status, Status::Continue);

    let (_, status) = conn.eom().await.unwrap();
    assert_eq!(status, Status::Continue);

    conn.close().await.unwrap();

    milter.shutdown().await.unwrap();
}
