mod common;

pub use common::*;

use indymilter_test::*;
use spamassassin_milter::*;

/// When no `spamd` server is available, `spamc` fails to connect.
#[tokio::test]
async fn spamc_connection_error() {
    let config = configure_spamc(Config::builder())
        .spamc_args([format!("--port={SPAMD_PORT}")])
        .build();

    let milter = SpamAssassinMilter::spawn(LOCALHOST, config).await.unwrap();

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("client.gluet.ch", [123, 123, 123, 123]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.helo("mail.gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.mail(["<from@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.rcpt(["<to@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    conn.macros(
        MacroStage::Data,
        [
            ("i", "1234567ABC"),
            ("j", "localhost"),
            ("_", "client.gluet.ch [123.123.123.123]"),
            ("{tls_version}", "TLSv1.2"),
            ("v", "Postfix 3.3.0"),
        ],
    )
    .await
    .unwrap();

    // When `spamc` cannot connect to `spamd`, it will retry several times. That
    // is why this test can proceed all the way to the `eom` stage where the
    // failure finally surfaces.

    let status = conn.data().await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("From", "from@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("To", "to@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Subject", "Test message").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Message-ID", rand_msg_id()).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Date", current_date()).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.eoh().await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.body(&b"Test message body"[..]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let (_, status) = conn.eom().await.unwrap();
    assert_eq!(status, Status::Tempfail { message: None });

    conn.close().await.unwrap();

    milter.shutdown().await.unwrap();
}
