mod common;

pub use common::*;

use indymilter_test::*;
use spamassassin_milter::*;

/// A connection from a trusted network is accepted.
#[tokio::test]
async fn trusted_network_connection() {
    let config = Config::builder()
        .trusted_network("123.120.0.0/14".parse().unwrap())
        .build();

    let milter = SpamAssassinMilter::spawn(LOCALHOST, config).await.unwrap();

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("client.gluet.ch", [123, 123, 123, 123]).await.unwrap();
    assert_eq!(status, Status::Accept);

    conn.close().await.unwrap();

    milter.shutdown().await.unwrap();
}
