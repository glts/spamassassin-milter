mod common;

pub use common::*;

use indymilter_test::*;
use spamassassin_milter::*;

/// Message body chunks are written to `spamc` until the maximum message size is
/// reached, the rest is skipped (oversized messages are not processed by
/// SpamAssassin, so it is futile to send the whole message in this case).
#[tokio::test]
async fn skip_oversized() {
    let config = configure_spamc(Config::builder())
        .max_message_size(512)
        .spamc_args([format!("--port={SPAMD_PORT}")])
        .build();

    let server = spawn_mock_spamd_server(SPAMD_PORT, Ok).await.unwrap();

    let milter = SpamAssassinMilter::spawn(LOCALHOST, config).await.unwrap();

    let mut conn = TestConnection::open(milter.addr()).await.unwrap();

    let status = conn.connect("client.gluet.ch", [123, 123, 123, 123]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.helo("mail.gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.mail(["<from@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.rcpt(["<to@gluet.ch>"]).await.unwrap();
    assert_eq!(status, Status::Continue);

    conn.macros(
        MacroStage::Data,
        [
            ("i", "1234567ABC"),
            ("j", "localhost"),
            ("_", "client.gluet.ch [123.123.123.123]"),
            ("{tls_version}", "TLSv1.2"),
            ("v", "Postfix 3.3.0"),
        ],
    )
    .await
    .unwrap();

    let status = conn.data().await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("From", "from@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("To", "to@gluet.ch").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Subject", "Test message").await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Message-ID", rand_msg_id()).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.header("Date", current_date()).await.unwrap();
    assert_eq!(status, Status::Continue);

    let status = conn.eoh().await.unwrap();
    assert_eq!(status, Status::Continue);

    // At this point still below the size limit …
    let status = conn.body(&b"Test message body"[..]).await.unwrap();
    assert_eq!(status, Status::Continue);

    // … after sending the following, we’re past the limit and skip.
    let status = conn.body(
        &b"\
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................"[..],
    )
    .await
    .unwrap();
    assert_eq!(status, Status::Skip);

    let (_, status) = conn.eom().await.unwrap();
    assert_eq!(status, Status::Continue);

    conn.close().await.unwrap();

    milter.shutdown().await.unwrap();

    server.await.unwrap().unwrap();
}
