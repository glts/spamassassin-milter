// SpamAssassin Milter – milter for spam filtering with SpamAssassin
// Copyright © 2020–2022 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use indymilter::{ActionError, SmtpReplyError};
use std::{
    error,
    fmt::{self, Display, Formatter},
    io, result,
};

pub type Result<T> = result::Result<T, Error>;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Error {
    ParseEmail,
    SmtpReply,
    Action,
    // For our purposes it is enough to record just the error message of I/O
    // errors, no need to keep the `io::Error` itself around.
    Io(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::ParseEmail => write!(f, "failed to parse email"),
            Self::SmtpReply => write!(f, "could not configure SMTP error reply"),
            Self::Action => write!(f, "could not execute milter context action"),
            Self::Io(msg) => msg.fmt(f),
        }
    }
}

impl error::Error for Error {}

impl From<SmtpReplyError> for Error {
    fn from(_: SmtpReplyError) -> Self {
        Self::SmtpReply
    }
}

impl From<ActionError> for Error {
    fn from(_: ActionError) -> Self {
        Self::Action
    }
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Self::Io(error.to_string())  // just record the error message
    }
}
