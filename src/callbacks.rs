// SpamAssassin Milter – milter for spam filtering with SpamAssassin
// Copyright © 2020–2024 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    client::{Client, ReceivedInfo, Spamc},
    config::Config,
};
use byte_strings::c_str;
use chrono::Local;
use indymilter::{
    Actions, Callbacks, Context, EomContext, MacroStage, Macros, NegotiateContext, ProtoOpts,
    SocketInfo, Status,
};
use std::{
    borrow::Cow,
    ffi::{CStr, CString},
    net::{IpAddr, Ipv4Addr},
    sync::Arc,
};

// The send operations all may fail with an I/O error. These are logged and
// answered with `Status::Tempfail`. We don’t expect this to happen in normal
// circumstances, only when something is wrong with `spamc` configuration or
// operation.
macro_rules! ok_or_tempfail {
    ($expr:expr) => {
        if let ::std::result::Result::Err(e) = $expr {
            eprintln!("failed to communicate with spamc: {e}");
            return ::indymilter::Status::Tempfail;
        }
    };
}

pub struct Connection {
    client_ip: IpAddr,
    helo_host: Option<Box<str>>,
    client: Option<Client>,
}

impl Connection {
    fn new(client_ip: IpAddr) -> Self {
        Self {
            client_ip,
            helo_host: None,
            client: None,
        }
    }
}

trait ConnectionMut {
    fn connection(&mut self) -> &mut Connection;
}

impl ConnectionMut for Option<Connection> {
    fn connection(&mut self) -> &mut Connection {
        self.as_mut().expect("milter context data not available")
    }
}

trait MacrosExt {
    fn get_string(&self, name: &CStr) -> Option<Cow<'_, str>>;

    fn queue_id(&self) -> Cow<'_, str> {
        self.get_string(c_str!("i"))
            .unwrap_or_else(|| "NONE".into())
    }
}

impl MacrosExt for Macros {
    fn get_string(&self, name: &CStr) -> Option<Cow<'_, str>> {
        self.get(name).map(|v| v.to_string_lossy())
    }
}

pub fn make_callbacks(config: Config) -> Callbacks<Connection> {
    let config = Arc::new(config);
    let config_connect = config.clone();
    let config_mail = config.clone();
    let config_body = config.clone();
    let config_eom = config.clone();

    Callbacks::new()
        .on_negotiate(move |cx, _, _| {
            Box::pin(handle_negotiate(config.clone(), cx))
        })
        .on_connect(move |cx, _, socket_info| {
            Box::pin(handle_connect(config_connect.clone(), cx, socket_info))
        })
        .on_helo(|cx, helo_host| Box::pin(handle_helo(cx, helo_host)))
        .on_mail(move |cx, smtp_args| {
            Box::pin(handle_mail(config_mail.clone(), cx, smtp_args))
        })
        .on_rcpt(|cx, smtp_args| Box::pin(handle_rcpt(cx, smtp_args)))
        .on_data(|cx| Box::pin(handle_data(cx)))
        .on_header(|cx, name, value| Box::pin(handle_header(cx, name, value)))
        .on_eoh(|cx| Box::pin(handle_eoh(cx)))
        .on_body(move |cx, chunk| {
            Box::pin(handle_body(config_body.clone(), cx, chunk))
        })
        .on_eom(move |cx| {
            Box::pin(handle_eom(config_eom.clone(), cx))
        })
        .on_abort(|cx| Box::pin(handle_abort(cx)))
        .on_close(|cx| Box::pin(handle_close(cx)))
}

async fn handle_negotiate(
    config: Arc<Config>,
    context: &mut NegotiateContext<Connection>,
) -> Status {
    if !config.dry_run() {
        context.requested_actions |= Actions::ADD_HEADER | Actions::CHANGE_HEADER;
        if !config.preserve_body() {
            context.requested_actions |= Actions::REPLACE_BODY;
        }
    }

    context.requested_opts |= ProtoOpts::SKIP | ProtoOpts::LEADING_SPACE;

    let macros = &mut context.requested_macros;
    macros.insert(MacroStage::Mail, c_str!("{auth_authen}").into());
    macros.insert(MacroStage::Data, c_str!("i j _ {tls_version} v").into());

    Status::Continue
}

async fn handle_connect(
    config: Arc<Config>,
    context: &mut Context<Connection>,
    socket_info: SocketInfo,
) -> Status {
    let ip = match socket_info {
        SocketInfo::Inet(addr) => addr.ip(),
        _ => IpAddr::V4(Ipv4Addr::LOCALHOST),
    };

    if config.use_trusted_networks() {
        if config.is_in_trusted_networks(&ip) {
            verbose!(config, "accepted connection from trusted network address {ip}");
            return Status::Accept;
        }
    } else if ip.is_loopback() {
        verbose!(config, "accepted local connection");
        return Status::Accept;
    }

    context.data = Some(Connection::new(ip));

    Status::Continue
}

async fn handle_helo(context: &mut Context<Connection>, helo_host: CString) -> Status {
    let conn = context.data.connection();

    let helo_host = helo_host.to_string_lossy();

    conn.helo_host = Some(helo_host.into());

    Status::Continue
}

async fn handle_mail(
    config: Arc<Config>,
    context: &mut Context<Connection>,
    smtp_args: Vec<CString>,
) -> Status {
    if !config.auth_untrusted() {
        if let Some(login) = context.macros.get_string(c_str!("{auth_authen}")) {
            verbose!(config, "accepted message from sender authenticated as \"{login}\"");
            return Status::Accept;
        }
    }

    let conn = context.data.connection();

    let spamc = Spamc::new(config.spamc_args());
    let sender = smtp_args[0].to_string_lossy();

    conn.client = Some(Client::new(spamc, sender.into()));

    Status::Continue
}

async fn handle_rcpt(context: &mut Context<Connection>, smtp_args: Vec<CString>) -> Status {
    let conn = context.data.connection();
    let client = conn.client.as_mut().unwrap();

    let recipient = smtp_args[0].to_string_lossy();

    client.add_recipient(recipient.into());

    Status::Continue
}

async fn handle_data(context: &mut Context<Connection>) -> Status {
    let conn = context.data.connection();
    let client = conn.client.as_mut().unwrap();

    let id = context.macros.queue_id();

    if let Err(e) = client.connect() {
        eprintln!("{id}: failed to start spamc: {e}");
        return Status::Tempfail;
    }

    // Note that when SpamAssassin reports are enabled (`report_safe 1`), the
    // synthesised headers below are ‘leaked’ to users in the sense that they
    // are included inside the email MIME attachment in the new message body.

    ok_or_tempfail!(client.send_envelope_sender().await);
    ok_or_tempfail!(client.send_envelope_recipients().await);

    let info = ReceivedInfo {
        client_ip: conn.client_ip,
        helo_host: conn.helo_host.as_deref(),
        client_name_addr: context.macros.get_string(c_str!("_")),
        my_hostname: context.macros.get_string(c_str!("j")),
        mta: context.macros.get_string(c_str!("v")),
        tls: context.macros.get_string(c_str!("{tls_version}")),
        auth: context.macros.get_string(c_str!("{auth_authen}")),
        queue_id: &id,
        date_time: Local::now().to_rfc2822(),
    };

    ok_or_tempfail!(client.send_synthesized_received_header(info).await);

    Status::Continue
}

async fn handle_header(context: &mut Context<Connection>, name: CString, value: CString) -> Status {
    let conn = context.data.connection();
    let client = conn.client.as_mut().unwrap();

    let name = name.to_string_lossy();
    let value = value.to_string_lossy();

    ok_or_tempfail!(client.send_header(&name, &value).await);

    Status::Continue
}

async fn handle_eoh(context: &mut Context<Connection>) -> Status {
    let conn = context.data.connection();
    let client = conn.client.as_mut().unwrap();

    ok_or_tempfail!(client.send_eoh().await);

    Status::Continue
}

async fn handle_body(
    config: Arc<Config>,
    context: &mut Context<Connection>,
    chunk: impl AsRef<[u8]>,
) -> Status {
    let conn = context.data.connection();
    let client = conn.client.as_mut().unwrap();

    ok_or_tempfail!(client.send_body_chunk(chunk.as_ref()).await);

    let max = config.max_message_size();
    if client.bytes_written() > max {
        let id = context.macros.queue_id();
        verbose!(config, "{id}: skipping rest of message larger than {max} bytes");
        client.skip_body();
        Status::Skip
    } else {
        Status::Continue
    }
}

async fn handle_eom(config: Arc<Config>, context: &mut EomContext<Connection>) -> Status {
    let conn = context.data.connection();
    let client = conn.client.take().unwrap();

    let id = context.macros.queue_id();

    match client.process(&id, &mut context.reply, &context.actions, &config).await {
        Ok(status) => status,
        Err(e) => {
            eprintln!("{id}: failed to process message: {e}");
            Status::Tempfail
        }
    }
}

async fn handle_abort(context: &mut Context<Connection>) -> Status {
    let conn = context.data.connection();

    conn.client = None;

    Status::Continue
}

async fn handle_close(context: &mut Context<Connection>) -> Status {
    context.data = None;

    Status::Continue
}
