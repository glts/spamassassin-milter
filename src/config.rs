// SpamAssassin Milter – milter for spam filtering with SpamAssassin
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use ipnet::IpNet;
use std::{collections::HashSet, net::IpAddr};

/// A builder for SpamAssassin Milter configuration objects.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct ConfigBuilder {
    use_trusted_networks: bool,
    trusted_networks: HashSet<IpNet>,
    auth_untrusted: bool,
    spamc_args: Vec<String>,
    max_message_size: usize,
    dry_run: bool,
    reject_spam: bool,
    reply_code: String,
    reply_status_code: String,
    reply_text: String,
    preserve_headers: bool,
    preserve_body: bool,
    verbose: bool,
}

impl ConfigBuilder {
    pub fn use_trusted_networks(mut self, value: bool) -> Self {
        self.use_trusted_networks = value;
        self
    }

    pub fn trusted_network(mut self, net: IpNet) -> Self {
        self.use_trusted_networks = true;
        self.trusted_networks.insert(net);
        self
    }

    pub fn auth_untrusted(mut self, value: bool) -> Self {
        self.auth_untrusted = value;
        self
    }

    pub fn spamc_args<I, S>(mut self, args: I) -> Self
    where
        I: IntoIterator<Item = S>,
        S: AsRef<str>,
    {
        self.spamc_args.extend(args.into_iter().map(|a| a.as_ref().to_owned()));
        self
    }

    pub fn max_message_size(mut self, value: usize) -> Self {
        self.max_message_size = value;
        self
    }

    pub fn dry_run(mut self, value: bool) -> Self {
        self.dry_run = value;
        self
    }

    pub fn reject_spam(mut self, value: bool) -> Self {
        self.reject_spam = value;
        self
    }

    pub fn reply_code(mut self, value: String) -> Self {
        self.reply_code = value;
        self
    }

    pub fn reply_status_code(mut self, value: String) -> Self {
        self.reply_status_code = value;
        self
    }

    pub fn reply_text(mut self, value: String) -> Self {
        self.reply_text = value;
        self
    }

    pub fn preserve_headers(mut self, value: bool) -> Self {
        self.preserve_headers = value;
        self
    }

    pub fn preserve_body(mut self, value: bool) -> Self {
        self.preserve_body = value;
        self
    }

    pub fn verbose(mut self, value: bool) -> Self {
        self.verbose = value;
        self
    }

    pub fn build(self) -> Config {
        // These invariants are enforced in `main`. In a future revision,
        // consider replacing the assertions with a `Result` return type.
        assert!(
            self.use_trusted_networks || self.trusted_networks.is_empty(),
            "trusted networks present but not used"
        );
        assert!(
            is_valid_reply_code(&self.reply_code)
                && is_valid_reply_code(&self.reply_status_code)
                && self.reply_code.as_bytes()[0] == self.reply_status_code.as_bytes()[0],
            "invalid or incompatible reply codes"
        );

        Config {
            use_trusted_networks: self.use_trusted_networks,
            trusted_networks: self.trusted_networks,
            auth_untrusted: self.auth_untrusted,
            spamc_args: self.spamc_args,
            max_message_size: self.max_message_size,
            dry_run: self.dry_run,
            reject_spam: self.reject_spam,
            reply_code: self.reply_code,
            reply_status_code: self.reply_status_code,
            reply_text: self.reply_text,
            preserve_headers: self.preserve_headers,
            preserve_body: self.preserve_body,
            verbose: self.verbose,
        }
    }
}

fn is_valid_reply_code(s: &str) -> bool {
    s.starts_with('4') || s.starts_with('5')
}

impl Default for ConfigBuilder {
    fn default() -> Self {
        Self {
            use_trusted_networks: Default::default(),
            trusted_networks: Default::default(),
            auth_untrusted: Default::default(),
            spamc_args: Default::default(),
            max_message_size: 512_000,  // same as in `spamc`
            dry_run: Default::default(),
            reject_spam: Default::default(),
            // This reply code and enhanced status code are the most appropriate
            // choices according to RFCs 5321 and 3463.
            reply_code: "550".into(),
            reply_status_code: "5.7.1".into(),
            // Generic reply text that makes no mention of SpamAssassin.
            reply_text: "Spam message refused".into(),
            preserve_headers: Default::default(),
            preserve_body: Default::default(),
            verbose: Default::default(),
        }
    }
}

/// A configuration object for SpamAssassin Milter.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Config {
    use_trusted_networks: bool,
    trusted_networks: HashSet<IpNet>,
    auth_untrusted: bool,
    spamc_args: Vec<String>,
    max_message_size: usize,
    dry_run: bool,
    reject_spam: bool,
    reply_code: String,
    reply_status_code: String,
    reply_text: String,
    preserve_headers: bool,
    preserve_body: bool,
    verbose: bool,
}

impl Config {
    pub fn builder() -> ConfigBuilder {
        Default::default()
    }

    pub fn use_trusted_networks(&self) -> bool {
        self.use_trusted_networks
    }

    pub fn is_in_trusted_networks(&self, ip: &IpAddr) -> bool {
        self.trusted_networks.iter().any(|n| n.contains(ip))
    }

    pub fn auth_untrusted(&self) -> bool {
        self.auth_untrusted
    }

    pub fn spamc_args(&self) -> &[String] {
        &self.spamc_args
    }

    pub fn max_message_size(&self) -> usize {
        self.max_message_size
    }

    pub fn dry_run(&self) -> bool {
        self.dry_run
    }

    pub fn reject_spam(&self) -> bool {
        self.reject_spam
    }

    pub fn reply_code(&self) -> &str {
        &self.reply_code
    }

    pub fn reply_status_code(&self) -> &str {
        &self.reply_status_code
    }

    pub fn reply_text(&self) -> &str {
        &self.reply_text
    }

    pub fn preserve_headers(&self) -> bool {
        self.preserve_headers
    }

    pub fn preserve_body(&self) -> bool {
        self.preserve_body
    }

    pub fn verbose(&self) -> bool {
        self.verbose
    }
}

impl Default for Config {
    fn default() -> Self {
        ConfigBuilder::default().build()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn trusted_networks_config() {
        let config = Config::builder()
            .trusted_network("127.0.0.1/8".parse().unwrap())
            .build();

        assert!(config.use_trusted_networks());
        assert!(config.is_in_trusted_networks(&"127.0.0.1".parse().unwrap()));
        assert!(!config.is_in_trusted_networks(&"10.1.0.1".parse().unwrap()));
    }

    #[test]
    fn spamc_args_extends_args() {
        let config = Config::builder()
            .spamc_args(["-p", "3030"])
            .spamc_args(["-x"])
            .build();

        assert_eq!(
            config.spamc_args(),
            &[String::from("-p"), String::from("3030"), String::from("-x")],
        );
    }
}
