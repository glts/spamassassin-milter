// SpamAssassin Milter – milter for spam filtering with SpamAssassin
// Copyright © 2020–2024 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use futures_util::stream::StreamExt;
use indymilter::EitherListener;
use signal_hook::consts::{SIGINT, SIGTERM};
use signal_hook_tokio::{Handle, Signals};
use spamassassin_milter::{Config, MILTER_NAME, VERSION};
use std::{
    env,
    error::Error,
    io::{stderr, stdout, Write},
    net::IpAddr,
    os::unix::fs::FileTypeExt,
    path::Path,
    process,
    str::FromStr,
};
use tokio::{
    fs,
    net::{TcpListener, UnixListener},
    sync::oneshot,
    task::JoinHandle,
};

const PROGRAM_NAME: &str = env!("CARGO_BIN_NAME");

#[tokio::main]
async fn main() {
    let (socket, config) = match parse_args() {
        Ok(config) => config,
        Err(e) => {
            let _ = writeln!(stderr(), "{PROGRAM_NAME}: {e}");
            process::exit(1);
        }
    };

    let (shutdown_tx, shutdown) = oneshot::channel();

    let signals = Signals::new([SIGTERM, SIGINT]).expect("failed to install signal handler");
    let signals_handle = signals.handle();
    let signals_task = spawn_signals_task(signals, shutdown_tx);

    let addr;
    let mut socket_path = None;
    let listener = match socket {
        Socket::Inet(addr) => {
            let listener = match TcpListener::bind(addr).await {
                Ok(listener) => listener,
                Err(e) => {
                    let _ = writeln!(stderr(), "{PROGRAM_NAME}: could not bind TCP socket: {e}");
                    process::exit(1);
                }
            };

            EitherListener::Tcp(listener)
        }
        Socket::Unix(path) => {
            // Before creating the socket file, try removing any existing socket
            // at the target path. This is to clear out a leftover file from a
            // previous, aborted execution.
            try_remove_socket(&path).await;

            let listener = match UnixListener::bind(path) {
                Ok(listener) => listener,
                Err(e) => {
                    let _ = writeln!(stderr(), "{PROGRAM_NAME}: could not create UNIX domain socket: {e}");
                    process::exit(1);
                }
            };

            // Remember the socket file path, and delete it on shutdown.
            addr = listener.local_addr().unwrap();
            socket_path = addr.as_pathname();

            EitherListener::Unix(listener)
        }
    };

    let result = spamassassin_milter::run(listener, config, shutdown).await;

    cleanup(signals_handle, signals_task, socket_path).await;

    if result.is_err() {
        process::exit(1);
    }
}

enum Socket {
    Inet(String),
    Unix(String),
}

impl FromStr for Socket {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(s) = s.strip_prefix("inet:") {
            Ok(Self::Inet(s.into()))
        } else if let Some(s) = s.strip_prefix("unix:") {
            Ok(Self::Unix(s.into()))
        } else {
            Err(format!("invalid value for socket: \"{s}\""))
        }
    }
}

const USAGE_TEXT: &str = "\
[OPTIONS] <SOCKET> [--] [<SPAMC_ARGS>...]

Arguments:
  <SOCKET>                          Listening socket of the milter
  <SPAMC_ARGS>...                   Additional arguments to pass to spamc

Options:
  -a, --auth-untrusted              Treat authenticated senders as untrusted
  -n, --dry-run                     Process messages without taking action
  -h, --help                        Print usage information
  -s, --max-message-size <BYTES>    Maximum message size to process
  -B, --preserve-body               Suppress rewriting of message body
  -H, --preserve-headers            Suppress rewriting of Subject/From/To headers
  -r, --reject-spam                 Reject messages flagged as spam
  -C, --reply-code <CODE>           Reply code when rejecting messages
  -S, --reply-status-code <CODE>    Status code when rejecting messages
  -R, --reply-text <MSG>            Reply text when rejecting messages
  -t, --trusted-networks <NETS>     Trust connections from these networks
  -v, --verbose                     Enable verbose operation logging
  -V, --version                     Print version information
";

fn parse_args() -> Result<(Socket, Config), Box<dyn Error>> {
    let mut args = env::args_os()
        .skip(1)
        .map(|s| s.into_string().map_err(|_| "invalid UTF-8 bytes in argument"));

    let mut config = Config::builder();

    let mut reply_code = None;
    let mut reply_status_code = None;

    let socket = loop {
        let arg = args.next().ok_or("required argument <SOCKET> missing")??;

        let missing_value = || format!("missing value for option {arg}");

        match arg.as_str() {
            "-h" | "--help" => {
                write!(stdout(), "Usage: {PROGRAM_NAME} {USAGE_TEXT}")?;
                process::exit(0);
            }
            "-V" | "--version" => {
                writeln!(stdout(), "{MILTER_NAME} {VERSION}")?;
                process::exit(0);
            }
            "-a" | "--auth-untrusted" => {
                config = config.auth_untrusted(true);
            }
            "-n" | "--dry-run" => {
                config = config.dry_run(true);
            }
            "-B" | "--preserve-body" => {
                config = config.preserve_body(true);
            }
            "-H" | "--preserve-headers" => {
                config = config.preserve_headers(true);
            }
            "-r" | "--reject-spam" => {
                config = config.reject_spam(true);
            }
            "-v" | "--verbose" => {
                config = config.verbose(true);
            }
            "-s" | "--max-message-size" => {
                let arg = args.next().ok_or_else(missing_value)??;
                let bytes = arg.parse()
                    .map_err(|_| format!("invalid value for max message size: \"{arg}\""))?;

                config = config.max_message_size(bytes);
            }
            "-C" | "--reply-code" => {
                let code = args.next().ok_or_else(missing_value)??;

                reply_code = Some(code);
            }
            "-S" | "--reply-status-code" => {
                let code = args.next().ok_or_else(missing_value)??;

                reply_status_code = Some(code);
            }
            "-R" | "--reply-text" => {
                let msg = args.next().ok_or_else(missing_value)??;

                config = config.reply_text(msg);
            }
            "-t" | "--trusted-networks" => {
                let arg = args.next().ok_or_else(missing_value)??;

                config = config.use_trusted_networks(true);

                for net in arg.split(',').filter(|n| !n.is_empty()) {
                    // Both `ipnet::IpNet` and `std::net::IpAddr` inputs are
                    // supported.
                    let net = net.parse()
                        .or_else(|_| net.parse::<IpAddr>().map(From::from))
                        .map_err(|_| format!("invalid value for trusted network address: \"{net}\""))?;

                    config = config.trusted_network(net);
                }
            }
            arg => {
                // Show a more helpful error message when the argument looks
                // like an option. (An option name can never be successfully
                // parsed as a socket.)
                if arg.starts_with('-')
                    && arg.chars().all(|c| c.is_ascii_alphanumeric() || c == '-')
                {
                    return Err(format!("unrecognized option: \"{arg}\"").into());
                }

                break arg.parse()?;
            }
        }
    };

    validate_reply_codes(reply_code.as_deref(), reply_status_code.as_deref())?;

    if let Some(code) = reply_code {
        config = config.reply_code(code);
    }
    if let Some(code) = reply_status_code {
        config = config.reply_status_code(code);
    }

    if let Some(arg) = args.next() {
        let arg = arg?;

        let mut spamc_args = if arg == "--" { vec![] } else { vec![arg] };

        for arg in args {
            spamc_args.push(arg?);
        }

        config = config.spamc_args(spamc_args);
    }

    Ok((socket, config.build()))
}

fn validate_reply_codes(
    reply_code: Option<&str>,
    reply_status_code: Option<&str>,
) -> Result<(), Box<dyn Error>> {
    match (reply_code, reply_status_code) {
        (Some(c1), Some(c2))
            if !((c1.starts_with('4') || c1.starts_with('5')) && c2.starts_with(&c1[..1])) =>
        {
            Err(format!(
                "invalid or incompatible values for reply code and status code: \"{c1}\", \"{c2}\""
            )
            .into())
        }
        (Some(c), None) if !c.starts_with('5') => {
            Err(format!("invalid value for reply code (5XX): \"{c}\"").into())
        }
        (None, Some(c)) if !c.starts_with('5') => {
            Err(format!("invalid value for reply status code (5.X.X): \"{c}\"").into())
        }
        _ => Ok(()),
    }
}

fn spawn_signals_task(
    mut signals: Signals,
    shutdown_milter: oneshot::Sender<()>,
) -> JoinHandle<()> {
    tokio::spawn(async move {
        if let Some(signal) = signals.next().await {
            match signal {
                SIGINT | SIGTERM => {
                    let _ = shutdown_milter.send(());
                }
                _ => panic!("unexpected signal"),
            }
        }
    })
}

async fn cleanup(signals_handle: Handle, signals_task: JoinHandle<()>, socket_path: Option<&Path>) {
    signals_handle.close();
    signals_task.await.expect("signal handler task failed");

    if let Some(p) = socket_path {
        try_remove_socket(p).await;
    }
}

async fn try_remove_socket(path: impl AsRef<Path>) {
    if let Ok(metadata) = fs::metadata(&path).await {
        if metadata.file_type().is_socket() {
            let _ = fs::remove_file(path).await;
        }
    }
}
