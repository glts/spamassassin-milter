// SpamAssassin Milter – milter for spam filtering with SpamAssassin
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    collections::{StrVecMap, StrVecSet},
    config::Config,
    error::{Error, Result},
};
use indymilter::ContextActions;
use once_cell::sync::Lazy;
use std::{
    ffi::CString,
    fmt::{self, Display, Formatter},
    str,
};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Header<'a> {
    pub name: &'a str,
    pub value: &'a str,
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Email<'a> {
    pub header: Vec<Header<'a>>,
    pub body: &'a [u8],
}

impl<'a> Email<'a> {
    pub fn parse(bytes: &'a [u8]) -> Result<Self> {
        let (header, body) = split_at_eoh(bytes)?;

        let header = header_lines(header)
            .into_iter()
            .map(parse_header_line)
            .collect::<Result<Vec<_>>>()?;

        Ok(Self { header, body })
    }
}

fn split_at_eoh(bytes: &[u8]) -> Result<(&[u8], &[u8])> {
    bytes
        .windows(4)
        .position(|w| w == b"\r\n\r\n")
        .map(|i| (&bytes[..(i + 2)], &bytes[(i + 4)..]))
        .ok_or(Error::ParseEmail)
}

fn header_lines(header: &[u8]) -> Vec<&[u8]> {
    let mut lines = Vec::new();

    let mut i = 0;
    let mut start = i;

    while i < header.len() {
        // Assume line endings are always encoded as b"\r\n" since that is what
        // the client sent to SpamAssassin earlier.
        if header[i] == b'\r' && i + 1 < header.len() && header[i + 1] == b'\n' {
            if i + 2 < header.len() && (header[i + 2] == b' ' || header[i + 2] == b'\t') {
                i += 3;
            } else {
                lines.push(&header[start..i]);
                i += 2;
                start = i;
            }
        } else {
            i += 1;
        }
    }

    if start != i {
        lines.push(&header[start..i]);
    }

    lines
}

fn parse_header_line(bytes: &[u8]) -> Result<Header<'_>> {
    // This assumes that headers received back from SpamAssassin are valid
    // UTF-8, which should be the case since the client only sent UTF-8 earlier.
    let line = str::from_utf8(bytes).map_err(|_| Error::ParseEmail)?;

    let (name, value) = line.split_at(line.find(':').ok_or(Error::ParseEmail)?);

    if name.trim().is_empty() {
        return Err(Error::ParseEmail);
    }

    let value = &value[1..];

    Ok(Header { name, value })
}

pub fn ensure_crlf(s: &str) -> String {
    // For symmetry, ensure existing occurrences of "\r\n" remain unchanged.
    s.split("\r\n")
        .flat_map(|s| s.split('\n'))
        .collect::<Vec<_>>()
        .join("\r\n")
}

pub fn ensure_lf(s: &str) -> String {
    s.replace("\r\n", "\n")
}

pub fn is_spam_assassin_header(name: &str) -> bool {
    let prefix = "X-Spam-";
    matches!(name.get(..prefix.len()), Some(s) if s.eq_ignore_ascii_case(prefix))
}

// Values use CRLF line breaks and include leading whitespace.
pub type HeaderMap = StrVecMap<Box<str>, Box<str>>;
pub type HeaderSet<'a> = StrVecSet<&'a str>;

pub static REWRITE_HEADERS: Lazy<HeaderSet<'static>> = Lazy::new(|| {
    let mut h = HeaderSet::new();
    h.insert("Subject");
    h.insert("From");
    h.insert("To");
    h
});

pub static REPORT_HEADERS: Lazy<HeaderSet<'static>> = Lazy::new(|| {
    let mut h = HeaderSet::new();
    h.insert("MIME-Version");
    h.insert("Content-Type");
    h
});

/// A header rewriter that processes headers returned by SpamAssassin, and
/// computes and applies modifications by referring back to the original set of
/// headers. The rewriter operates only on the first occurrence of headers with
/// the same name.
#[derive(Clone, Debug)]
pub struct HeaderRewriter<'a, 'c> {
    original: HeaderMap,
    processed: HeaderSet<'a>,
    prepend: Option<bool>,
    spam_flag: Option<bool>,
    spam_assassin_mods: Vec<HeaderMod<'a>>,
    rewrite_mods: Vec<HeaderMod<'a>>,
    report_mods: Vec<HeaderMod<'a>>,
    config: &'c Config,
}

impl<'a, 'c> HeaderRewriter<'a, 'c> {
    pub fn new(original: HeaderMap, config: &'c Config) -> Self {
        Self {
            original,
            processed: HeaderSet::new(),
            prepend: None,
            spam_flag: None,
            spam_assassin_mods: vec![],
            rewrite_mods: vec![],
            report_mods: vec![],
            config,
        }
    }

    pub fn process_header(&mut self, name: &'a str, value: &'a str) {
        // The very first header seen determines if ‘X-Spam-’ headers are
        // prepended or appended to the existing header.
        self.prepend.get_or_insert_with(|| is_spam_assassin_header(name));

        if name.eq_ignore_ascii_case("X-Spam-Flag") {
            self.spam_flag.get_or_insert_with(|| value.trim().eq_ignore_ascii_case("YES"));
        }

        if is_spam_assassin_header(name) {
            if let Some(m) = self.convert_to_x_spam_header_mod(name, value) {
                self.spam_assassin_mods.push(m);
            }
        } else if REWRITE_HEADERS.contains(name) {
            if let Some(m) = self.convert_to_header_mod(name, value) {
                self.rewrite_mods.push(m);
            }
        } else if REPORT_HEADERS.contains(name) {
            if let Some(m) = self.convert_to_header_mod(name, value) {
                self.report_mods.push(m);
            }
        }
    }

    fn convert_to_x_spam_header_mod(
        &mut self,
        name: &'a str,
        value: &'a str,
    ) -> Option<HeaderMod<'a>> {
        if !self.processed.insert(name) {
            return None;
        }

        let prepend = self.prepend.unwrap();

        match self.original.get(name) {
            None => Some(HeaderMod::Add { name, value, prepend }),
            Some(original_value) => {
                // Special case ‘X-Spam-Prev-’ headers: if already present,
                // modify them in place or leave them be. Other ‘X-Spam-’
                // headers are replaced, ie stripped and re-added.
                let prev = "X-Spam-Prev-";
                if matches!(name.get(..prev.len()), Some(s) if s.eq_ignore_ascii_case(prev)) {
                    if original_value.as_ref() != value {
                        Some(HeaderMod::Modify { name, value })
                    } else {
                        None
                    }
                } else {
                    Some(HeaderMod::Replace { name, value, prepend })
                }
            }
        }
    }

    fn convert_to_header_mod(&mut self, name: &'a str, value: &'a str) -> Option<HeaderMod<'a>> {
        if !self.processed.insert(name) {
            return None;
        }

        match self.original.get(name) {
            None => Some(HeaderMod::Add { name, value, prepend: false }),
            Some(original_value) => {
                if original_value.as_ref() != value {
                    Some(HeaderMod::Modify { name, value })
                } else {
                    None
                }
            }
        }
    }

    pub fn is_flagged_spam(&self) -> bool {
        self.spam_flag.unwrap_or(false)
    }

    pub async fn rewrite_spam_assassin_headers(
        &self,
        id: &str,
        actions: &impl ContextActions,
    ) -> Result<()> {
        let mods = self.spam_assassin_mods.iter();
        if self.prepend.unwrap_or(false) {
            // Prepend ‘X-Spam-’ headers in reverse order, so that they appear
            // in the order received from SpamAssassin.
            execute_mods(id, mods.rev(), actions, self.config).await?;
        } else {
            execute_mods(id, mods, actions, self.config).await?;
        }

        // Delete all incoming ‘X-Spam-’ headers not returned by SpamAssassin to
        // get rid of foreign ‘X-Spam-Flag’ etc. headers.
        let deletions = self
            .original
            .keys()
            .filter(|n| is_spam_assassin_header(n) && !self.processed.contains(n))
            .map(|name| HeaderMod::Delete { name })
            .collect::<Vec<_>>();
        execute_mods(id, deletions.iter(), actions, self.config).await
    }

    pub async fn rewrite_rewrite_headers(
        &self,
        id: &str,
        actions: &impl ContextActions,
    ) -> Result<()> {
        execute_mods(id, self.rewrite_mods.iter(), actions, self.config).await
    }

    pub async fn rewrite_report_headers(
        &self,
        id: &str,
        actions: &impl ContextActions,
    ) -> Result<()> {
        execute_mods(id, self.report_mods.iter(), actions, self.config).await
    }
}

async fn execute_mods<'a, I>(
    id: &str,
    mods: I,
    actions: &impl ContextActions,
    config: &Config,
) -> Result<()>
where
    I: IntoIterator<Item = &'a HeaderMod<'a>>,
{
    for m in mods {
        if config.dry_run() {
            verbose!(config, "{id}: rewriting header: {m} [dry run, not done]");
        } else {
            verbose!(config, "{id}: rewriting header: {m}");
            m.execute(actions).await?;
        }
    }
    Ok(())
}

pub async fn replace_body(
    id: &str,
    body: &[u8],
    actions: &impl ContextActions,
    config: &Config,
) -> Result<()> {
    if config.dry_run() {
        verbose!(config, "{id}: replacing message body [dry run, not done]");
    } else {
        verbose!(config, "{id}: replacing message body");
        actions.replace_body(body).await?;
    }
    Ok(())
}

/// A header rewriting modification operation. These are intended to operate
/// only on the first instance of headers occurring multiple times.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
enum HeaderMod<'a> {
    Add { name: &'a str, value: &'a str, prepend: bool },
    Replace { name: &'a str, value: &'a str, prepend: bool },
    Modify { name: &'a str, value: &'a str },
    Delete { name: &'a str },
}

impl HeaderMod<'_> {
    async fn execute(&self, actions: &impl ContextActions) -> Result<()> {
        // The milter protocol is smart enough to treat the name in a
        // case-insensitive manner, eg ‘Subject’ may replace ‘sUbject’.
        match *self {
            Self::Add { name, value, prepend } => add_header(actions, name, value, prepend).await?,
            Self::Replace { name, value, prepend } => {
                delete_header(actions, name).await?;
                add_header(actions, name, value, prepend).await?;
            }
            Self::Modify { name, value } => {
                actions.change_header(name, 1, Some(ensure_lf(value))).await?;
            }
            Self::Delete { name } => delete_header(actions, name).await?,
        }
        Ok(())
    }
}

async fn add_header(
    actions: &impl ContextActions,
    name: &str,
    value: &str,
    prepend: bool,
) -> Result<()> {
    if prepend {
        actions.insert_header(0, name, ensure_lf(value)).await?;
    } else {
        actions.add_header(name, ensure_lf(value)).await?;
    }
    Ok(())
}

async fn delete_header(actions: &impl ContextActions, name: &str) -> Result<()> {
    actions.change_header(name, 1, None::<CString>).await?;
    Ok(())
}

impl Display for HeaderMod<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use HeaderMod::*;

        match self {
            Add { name, .. } => write!(f, "add header \"{name}\""),
            Replace { name, .. } | Modify { name, .. } => {
                write!(f, "replace header \"{name}\"")
            }
            Delete { name } => write!(f, "delete header \"{name}\""),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn email_split_at_eoh() {
        assert_eq!(split_at_eoh(b"x\r\n\r\ny"), Ok((&b"x\r\n"[..], &b"y"[..])));
        assert_eq!(split_at_eoh(b"x\r\n\r\n"), Ok((&b"x\r\n"[..], &b""[..])));
        assert_eq!(split_at_eoh(b"\r\n\r\ny"), Ok((&b"\r\n"[..], &b"y"[..])));
        assert_eq!(split_at_eoh(b"\r\ny"), Err(Error::ParseEmail));
        assert_eq!(split_at_eoh(b"y"), Err(Error::ParseEmail));
    }

    #[test]
    fn email_header_lines_empty() {
        assert_eq!(header_lines(b""), Vec::<&[_]>::new());
        assert_eq!(header_lines(b"\r\n"), vec![&b""[..]]);
        assert_eq!(header_lines(b"\r\n\r\n"), vec![&b""[..], &b""[..]]);
    }

    #[test]
    fn email_header_lines_simple() {
        assert_eq!(header_lines(b"x\r\n"), vec![&b"x"[..]]);
        assert_eq!(header_lines(b"x\r\ny"), vec![&b"x"[..], &b"y"[..]]);
        assert_eq!(header_lines(b"x\r\ny\r\n"), vec![&b"x"[..], &b"y"[..]]);
    }

    #[test]
    fn email_header_lines_multi() {
        assert_eq!(header_lines(b"x\r\n\t"), vec![&b"x\r\n\t"[..]]);
        assert_eq!(header_lines(b"x\r\n\ty"), vec![&b"x\r\n\ty"[..]]);
        assert_eq!(header_lines(b"x\r\n\ty\r\n"), vec![&b"x\r\n\ty"[..]]);
        assert_eq!(
            header_lines(b"x\r\n\ty\r\n\tz\r\nq"),
            vec![&b"x\r\n\ty\r\n\tz"[..], &b"q"[..]]
        );
    }

    #[test]
    fn email_parse_header_line() {
        assert_eq!(parse_header_line(b"no colon"), Err(Error::ParseEmail));
        assert_eq!(parse_header_line(b":empty name"), Err(Error::ParseEmail));
        assert_eq!(parse_header_line(b"\t : whitespace name"), Err(Error::ParseEmail));
        assert_eq!(parse_header_line(b"name:value"), Ok(Header { name: "name", value: "value" }));
        assert_eq!(parse_header_line(b"name: value"), Ok(Header { name: "name", value: " value" }));
        assert_eq!(
            parse_header_line(b"name:\r\n\tvalue"),
            Ok(Header { name: "name", value: "\r\n\tvalue" })
        );
    }

    #[test]
    fn ensure_crlf_ok() {
        assert_eq!(&ensure_crlf(""), "");
        assert_eq!(&ensure_crlf("\n"), "\r\n");
        assert_eq!(&ensure_crlf("\r\n"), "\r\n");
        assert_eq!(&ensure_crlf("a\nb"), "a\r\nb");
        assert_eq!(&ensure_crlf("a\n\nb"), "a\r\n\r\nb");
        assert_eq!(&ensure_crlf("a\r\n\nb"), "a\r\n\r\nb");
        assert_eq!(&ensure_crlf("a\n\r\nb"), "a\r\n\r\nb");
        assert_eq!(&ensure_crlf("a\r\n\r\nb"), "a\r\n\r\nb");
        assert_eq!(&ensure_crlf("a\r\n\n\r\nb"), "a\r\n\r\n\r\nb");
        assert_eq!(&ensure_crlf("a\r\nb\n"), "a\r\nb\r\n");
    }

    #[test]
    fn ensure_lf_ok() {
        assert_eq!(&ensure_lf(""), "");
        assert_eq!(&ensure_lf("\n"), "\n");
        assert_eq!(&ensure_lf("\r\n"), "\n");
        assert_eq!(&ensure_lf("a\nb"), "a\nb");
        assert_eq!(&ensure_lf("a\n\nb"), "a\n\nb");
        assert_eq!(&ensure_lf("a\r\n\nb"), "a\n\nb");
        assert_eq!(&ensure_lf("a\n\r\nb"), "a\n\nb");
        assert_eq!(&ensure_lf("a\r\nb\n"), "a\nb\n");
    }

    #[test]
    fn spam_assassin_header_predicate() {
        assert!(is_spam_assassin_header("x-spam-status"));
        assert!(is_spam_assassin_header("x-spam-bogus"));
        assert!(is_spam_assassin_header("x-spam-"));
        assert!(!is_spam_assassin_header("x-spam"));
        assert!(!is_spam_assassin_header("bogus"));
    }

    #[test]
    fn header_rewriter_flags_spam() {
        let mut headers = HeaderMap::new();
        headers.insert("x-spam-flag".into(), " no".into());
        let config = Default::default();

        let mut rewriter = HeaderRewriter::new(headers, &config);
        rewriter.process_header("X-Spam-Flag", " YES");

        assert!(rewriter.is_flagged_spam());
    }

    #[test]
    fn header_rewriter_processes_first_occurrence_only() {
        let headers = HeaderMap::new();
        let config = Default::default();

        let mut rewriter = HeaderRewriter::new(headers, &config);
        rewriter.process_header("X-Spam-Flag", " NO");
        rewriter.process_header("X-Spam-Flag", " YES");

        assert_eq!(
            rewriter.spam_assassin_mods,
            [
                HeaderMod::Add {
                    name: "X-Spam-Flag",
                    value: " NO",
                    prepend: true,
                },
            ]
        );
    }

    #[test]
    fn header_rewriter_adds_and_replaces_headers() {
        let mut headers = HeaderMap::new();
        headers.insert("x-spam-level".into(), " ***".into());
        headers.insert("subject".into(), " original".into());
        headers.insert("x-spam-prev-subject".into(), " very original".into());
        headers.insert("to".into(), " recipient@gluet.ch".into());
        let config = Default::default();

        let mut rewriter = HeaderRewriter::new(headers, &config);
        rewriter.process_header("X-Spam-Level", " *****");
        rewriter.process_header("X-Spam-Report", " report");
        rewriter.process_header("Subject", " [SPAM] original");
        rewriter.process_header("X-Spam-Prev-Subject", " very original");
        rewriter.process_header("From", " sender@gluet.ch");
        rewriter.process_header("To", " recipient@gluet.ch");

        assert_eq!(
            rewriter.spam_assassin_mods,
            [
                HeaderMod::Replace {
                    name: "X-Spam-Level",
                    value: " *****",
                    prepend: true,
                },
                HeaderMod::Add {
                    name: "X-Spam-Report",
                    value: " report",
                    prepend: true,
                },
            ]
        );
        assert_eq!(
            rewriter.rewrite_mods,
            [
                HeaderMod::Modify {
                    name: "Subject",
                    value: " [SPAM] original",
                },
                HeaderMod::Add {
                    name: "From",
                    value: " sender@gluet.ch",
                    prepend: false,
                },
            ]
        );
    }

    #[test]
    fn header_rewriter_adds_appended_headers() {
        let headers = HeaderMap::new();
        let config = Default::default();

        let mut rewriter = HeaderRewriter::new(headers, &config);
        rewriter.process_header("Received", " from localhost by ...");
        rewriter.process_header("X-Spam-Flag", " YES");

        assert_eq!(
            rewriter.spam_assassin_mods,
            [
                HeaderMod::Add {
                    name: "X-Spam-Flag",
                    value: " YES",
                    prepend: false,
                },
            ]
        );
    }
}
