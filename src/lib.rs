// SpamAssassin Milter – milter for spam filtering with SpamAssassin
// Copyright © 2020–2024 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

//! The SpamAssassin Milter application library.
//!
//! This library was published to facilitate integration testing of the
//! [SpamAssassin Milter application][SpamAssassin Milter]. No backwards
//! compatibility guarantees are made for the public API in this library. Please
//! look into the application instead.
//!
//! [SpamAssassin Milter]: https://crates.io/crates/spamassassin-milter

// The standard `eprintln` macro is replaced throughout with a best-effort,
// non-panicking version.
macro_rules! eprintln {
    ($($arg:tt)*) => {
        {
            use ::std::io::Write;
            let _ = ::std::writeln!(::std::io::stderr(), $($arg)*);
        }
    };
}

macro_rules! verbose {
    ($config:ident, $($arg:tt)*) => {
        if $config.verbose() {
            // Note: not qualifying `eprintln!` here makes it use textual scope
            // and thus refer to above definition.
            eprintln!($($arg)*);
        }
    };
}

mod callbacks;
mod client;
mod collections;
mod config;
mod email;
mod error;

pub use crate::config::{Config, ConfigBuilder};
use indymilter::Listener;
use std::{future::Future, io};

/// The name of the SpamAssassin Milter application.
pub const MILTER_NAME: &str = "SpamAssassin Milter";

/// The current version string of SpamAssassin Milter.
pub const VERSION: &str = env!("CARGO_PKG_VERSION");

/// Starts SpamAssassin Milter listening on the given socket using the supplied
/// configuration.
///
/// # Errors
///
/// If execution of the milter fails, an error is returned.
///
/// # Examples
///
/// ```
/// # async fn f() -> std::io::Result<()> {
/// use std::process;
/// use tokio::{net::TcpListener, signal};
///
/// let listener = TcpListener::bind("127.0.0.1:3000").await?;
/// let config = Default::default();
/// let shutdown = signal::ctrl_c();
///
/// if let Err(e) = spamassassin_milter::run(listener, config, shutdown).await {
///     eprintln!("failed to run spamassassin-milter: {e}");
///     process::exit(1);
/// }
/// # Ok(())
/// # }
/// ```
pub async fn run(
    listener: impl Listener,
    config: Config,
    shutdown: impl Future,
) -> io::Result<()> {
    let callbacks = callbacks::make_callbacks(config);
    let config = Default::default();

    eprintln!("{MILTER_NAME} {VERSION} starting");

    let result = indymilter::run(listener, callbacks, config, shutdown).await;

    match &result {
        Ok(()) => eprintln!("{MILTER_NAME} {VERSION} shut down"),
        Err(e) => eprintln!("{MILTER_NAME} {VERSION} terminated with error: {e}"),
    }

    result
}
