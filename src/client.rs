// SpamAssassin Milter – milter for spam filtering with SpamAssassin
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    config::Config,
    email::{self, Email, HeaderMap, HeaderRewriter},
    error::{Error, Result},
};
use async_trait::async_trait;
use indymilter::{ContextActions, SetErrorReply, Status};
use std::{
    any::Any, borrow::Cow, io, net::IpAddr, os::unix::process::ExitStatusExt, pin::Pin,
    process::Stdio,
};
use tokio::{
    io::{AsyncReadExt, AsyncWrite, AsyncWriteExt},
    process::{Child, Command},
    task::JoinHandle,
};

#[async_trait]
pub trait Process {
    fn connect(&mut self) -> Result<()>;
    fn writer(&mut self) -> Pin<Box<dyn AsyncWrite + Send + '_>>;
    async fn finish(&mut self) -> Result<Vec<u8>>;
    fn as_any(&self) -> &dyn Any;  // for testing only
}

pub struct Spamc {
    spamc_args: Vec<String>,
    spamc: Option<Child>,
    stdout_reader: Option<JoinHandle<io::Result<Vec<u8>>>>,
}

impl Spamc {
    const SPAMC_PROGRAM: &'static str = match option_env!("SPAMASSASSIN_MILTER_SPAMC") {
        Some(p) => p,
        None => "/usr/bin/spamc",
    };

    pub fn new(spamc_args: &[String]) -> Self {
        Self {
            spamc_args: spamc_args.into(),
            spamc: None,
            stdout_reader: None,
        }
    }
}

#[async_trait]
impl Process for Spamc {
    fn connect(&mut self) -> Result<()> {
        // `Command::spawn` always succeeds when `spamc` can be invoked, even if
        // logically the command is invalid, eg if it uses non-existing options.
        let mut spamc = Command::new(Self::SPAMC_PROGRAM)
            .args(&self.spamc_args)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .kill_on_drop(true)
            .spawn()?;

        let mut stdout = spamc.stdout.take().unwrap();

        self.spamc = Some(spamc);

        // When processing large messages, `spamc` may begin to write its
        // response to stdout while it is still receiving parts of the message
        // body. Avoid blocking by reading stdout in a separate task.
        self.stdout_reader = Some(tokio::spawn(async move {
            let mut output = Vec::new();
            stdout.read_to_end(&mut output).await?;
            Ok(output)
        }));

        Ok(())
    }

    fn writer(&mut self) -> Pin<Box<dyn AsyncWrite + Send + '_>> {
        let spamc = self.spamc.as_mut().expect("spamc process not started");

        Box::pin(spamc.stdin.as_mut().unwrap())
    }

    async fn finish(&mut self) -> Result<Vec<u8>> {
        let mut spamc = self.spamc.take().expect("spamc process not started");

        let status = spamc.wait().await?;

        let stdout = self
            .stdout_reader
            .take()
            .expect("spamc stdout reader task not available")
            .await
            .expect("panic in spamc stdout reader task")?;

        if status.success() {
            Ok(stdout)
        } else {
            Err(match status.code() {
                None => Error::Io(format!(
                    "spamc terminated by signal {}",
                    status.signal().unwrap()
                )),
                Some(code) => Error::Io(format!("spamc exited with status code {code}")),
            })
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

pub struct ReceivedInfo<'helo, 'macros> {
    pub client_ip: IpAddr,
    pub helo_host: Option<&'helo str>,
    pub client_name_addr: Option<Cow<'macros, str>>,
    pub my_hostname: Option<Cow<'macros, str>>,
    pub mta: Option<Cow<'macros, str>>,
    pub tls: Option<Cow<'macros, str>>,
    pub auth: Option<Cow<'macros, str>>,
    pub queue_id: &'macros str,
    pub date_time: String,
}

pub struct Client {
    process: Box<dyn Process + Send>,
    sender: Box<str>,
    recipients: Vec<Box<str>>,
    headers: HeaderMap,
    bytes: usize,
    skipped: bool,
}

impl Client {
    pub fn new(process: impl Process + Send + 'static, sender: Box<str>) -> Self {
        Self {
            process: Box::new(process),
            sender,
            recipients: vec![],
            headers: HeaderMap::new(),
            bytes: 0,
            skipped: false,
        }
    }

    pub fn add_recipient(&mut self, recipient: Box<str>) {
        self.recipients.push(recipient);
    }

    pub fn bytes_written(&self) -> usize {
        self.bytes
    }

    pub fn skip_body(&mut self) {
        self.skipped = true;
    }

    pub fn connect(&mut self) -> Result<()> {
        self.process.connect()
    }

    pub async fn send_envelope_sender(&mut self) -> Result<()> {
        let buf = format!("X-Envelope-From: {}\r\n", self.sender);

        self.process.writer().write_all(buf.as_bytes()).await?;
        self.bytes += buf.len();

        Ok(())
    }

    pub async fn send_envelope_recipients(&mut self) -> Result<()> {
        let buf = format!("X-Envelope-To: {}\r\n", self.recipients.join(",\r\n\t"));

        self.process.writer().write_all(buf.as_bytes()).await?;
        self.bytes += buf.len();

        Ok(())
    }

    pub async fn send_synthesized_received_header(
        &mut self,
        info: ReceivedInfo<'_, '_>,
    ) -> Result<()> {
        // Sending this ‘Received’ header is crucial: Milters don’t see the
        // MTA’s own ‘Received’ header. However, SpamAssassin draws a lot of
        // information from that header. So we make one up and send it along.

        let client_ip = info.client_ip.to_string();
        let helo_host = info.helo_host.unwrap_or(&client_ip);

        let client_name_addr = info.client_name_addr.as_deref().unwrap_or(&client_ip);
        let my_hostname = info.my_hostname.as_deref().unwrap_or("localhost");
        let mta = info
            .mta
            .as_ref()
            .and_then(|v| v.split_ascii_whitespace().next())
            .unwrap_or("Postfix");

        let buf = format!(
            "Received: from {helo} ({client})\r\n\
             \tby {hostname} ({mta}) with ESMTP{tls}{auth} id {id};\r\n\
             \t{date_time}\r\n\
             \t(envelope-from {sender})\r\n",
            helo = helo_host,
            client = client_name_addr,
            hostname = my_hostname,
            mta = mta,
            tls = if info.tls.is_some() { "S" } else { "" },
            auth = if info.auth.is_some() { "A" } else { "" },
            id = info.queue_id,
            date_time = info.date_time,
            sender = self.sender
        );

        self.process.writer().write_all(buf.as_bytes()).await?;
        self.bytes += buf.len();

        Ok(())
    }

    pub async fn send_header(&mut self, name: &str, value: &str) -> Result<()> {
        // As requested during milter protocol negotiation, the value includes
        // leading whitespace. This lets us pass on whitespace exactly as is.
        let value = email::ensure_crlf(value);
        let buf = format!("{name}:{value}\r\n");

        if email::is_spam_assassin_header(name)
            || email::REWRITE_HEADERS.contains(name)
            || email::REPORT_HEADERS.contains(name)
        {
            self.headers.insert_if_absent(name, value.into());
        }

        self.process.writer().write_all(buf.as_bytes()).await?;
        self.bytes += buf.len();

        Ok(())
    }

    pub async fn send_eoh(&mut self) -> Result<()> {
        let eoh = b"\r\n";

        self.process.writer().write_all(eoh).await?;
        self.bytes += eoh.len();

        Ok(())
    }

    pub async fn send_body_chunk(&mut self, bytes: &[u8]) -> Result<()> {
        self.process.writer().write_all(bytes).await?;
        self.bytes += bytes.len();

        Ok(())
    }

    pub async fn process(
        mut self,
        id: &str,
        reply: &mut impl SetErrorReply,
        actions: &impl ContextActions,
        config: &Config,
    ) -> Result<Status> {
        let output = match self.process.finish().await {
            Ok(output) => output,
            Err(e) => {
                eprintln!("{id}: failed to complete spamc communication: {e}");
                return Ok(Status::Tempfail);
            }
        };

        let email = match Email::parse(&output) {
            Ok(email) => email,
            Err(e) => {
                eprintln!("{id}: invalid response from spamc: {e}");
                return Ok(Status::Tempfail);
            }
        };

        let mut rewriter = HeaderRewriter::new(self.headers, config);
        for header in email.header {
            rewriter.process_header(header.name, header.value);
        }

        let spam = rewriter.is_flagged_spam();

        if spam && config.reject_spam() {
            return reject_spam(id, reply, config);
        }

        rewriter.rewrite_spam_assassin_headers(id, actions).await?;

        if spam {
            if !config.preserve_headers() {
                rewriter.rewrite_rewrite_headers(id, actions).await?;
            }
            if !config.preserve_body() {
                rewriter.rewrite_report_headers(id, actions).await?;
                replace_body(id, email.body, self.skipped, actions, config).await?;
            }
        }

        Ok(Status::Continue)
    }
}

fn reject_spam(id: &str, reply: &mut impl SetErrorReply, config: &Config) -> Result<Status> {
    if config.dry_run() {
        verbose!(config, "{id}: rejected message flagged as spam [dry run, not done]");
        Ok(Status::Accept)
    } else {
        reply.set_error_reply(
            config.reply_code(),
            Some(config.reply_status_code()),
            config.reply_text().lines(),
        )?;

        verbose!(config, "{id}: rejected message flagged as spam");
        Ok(if config.reply_code().starts_with('5') {
            Status::Reject
        } else {
            Status::Tempfail
        })
    }
}

async fn replace_body(
    id: &str,
    body: &[u8],
    skipped: bool,
    actions: &impl ContextActions,
    config: &Config,
) -> Result<()> {
    // Do not replace the message body when part of it was skipped. This only
    // occurs when the milter’s max message size is less than that of `spamc`.
    // This condition ensures message integrity in such a misconfigured setup.
    if skipped {
        eprintln!(
            "{id}: not replacing possibly truncated message body; \
            please review max message size setting ({})",
            config.max_message_size()
        );
    } else {
        email::replace_body(id, body, actions, config).await?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use byte_strings::c_str;
    use indymilter::{ActionError, IntoCString, SmtpReplyError};
    use std::{ffi::CString, result, sync::Mutex};

    #[derive(Debug, Default)]
    struct MockSpamc {
        buf: Vec<u8>,
        output: Option<Vec<u8>>,
    }

    impl MockSpamc {
        fn new() -> Self {
            Default::default()
        }

        fn with_output(output: Vec<u8>) -> Self {
            Self {
                output: Some(output),
                ..Default::default()
            }
        }
    }

    #[async_trait]
    impl Process for MockSpamc {
        fn connect(&mut self) -> Result<()> {
            Ok(())
        }

        fn writer(&mut self) -> Pin<Box<dyn AsyncWrite + Send + '_>> {
            Box::pin(&mut self.buf)
        }

        async fn finish(&mut self) -> Result<Vec<u8>> {
            Ok(if let Some(output) = &self.output {
                output.clone()
            } else {
                self.buf.clone()
            })
        }

        fn as_any(&self) -> &dyn Any {
            self
        }
    }

    fn as_mock_spamc(process: &dyn Process) -> &MockSpamc {
        process.as_any().downcast_ref().unwrap()
    }

    #[derive(Debug, Eq, PartialEq)]
    enum Action {
        AddHeader(CString, CString),
        InsertHeader(i32, CString, CString),
        ChangeHeader(CString, i32, Option<CString>),
        ReplaceBody(Vec<u8>),
    }

    #[derive(Debug, Default)]
    struct MockEomActions {
        called: Mutex<Vec<Action>>,
    }

    impl MockEomActions {
        fn new() -> Self {
            Default::default()
        }
    }

    #[async_trait]
    impl ContextActions for MockEomActions {
        async fn add_header<'cx, 'k, 'v>(
            &'cx self,
            name: impl IntoCString + Send + 'k,
            value: impl IntoCString + Send + 'v,
        ) -> result::Result<(), ActionError> {
            let action = Action::AddHeader(name.into_c_string(), value.into_c_string());
            self.called.lock().unwrap().push(action);
            Ok(())
        }

        async fn insert_header<'cx, 'k, 'v>(
            &'cx self,
            index: i32,
            name: impl IntoCString + Send + 'k,
            value: impl IntoCString + Send + 'v,
        ) -> result::Result<(), ActionError> {
            let action = Action::InsertHeader(index, name.into_c_string(), value.into_c_string());
            self.called.lock().unwrap().push(action);
            Ok(())
        }

        async fn change_header<'cx, 'k, 'v>(
            &'cx self,
            name: impl IntoCString + Send + 'k,
            index: i32,
            value: Option<impl IntoCString + Send + 'v>,
        ) -> result::Result<(), ActionError> {
            let action = Action::ChangeHeader(
                name.into_c_string(),
                index,
                value.map(|v| v.into_c_string()),
            );
            self.called.lock().unwrap().push(action);
            Ok(())
        }

        async fn replace_body<'cx, 'a>(
            &'cx self,
            chunk: &'a [u8],
        ) -> result::Result<(), ActionError> {
            let action = Action::ReplaceBody(chunk.to_vec());
            self.called.lock().unwrap().push(action);
            Ok(())
        }

        async fn change_sender<'cx, 'a, 'b>(
            &'cx self,
            _: impl IntoCString + Send + 'a,
            _: Option<impl IntoCString + Send + 'b>,
        ) -> result::Result<(), ActionError> {
            unimplemented!()
        }

        async fn add_recipient<'cx, 'a>(
            &'cx self,
            _: impl IntoCString + Send + 'a,
        ) -> result::Result<(), ActionError> {
            unimplemented!()
        }

        async fn add_recipient_ext<'cx, 'a, 'b>(
            &'cx self,
            _: impl IntoCString + Send + 'a,
            _: Option<impl IntoCString + Send + 'b>,
        ) -> result::Result<(), ActionError> {
            unimplemented!()
        }

        async fn delete_recipient<'cx, 'a>(
            &'cx self,
            _: impl IntoCString + Send + 'a,
        ) -> result::Result<(), ActionError> {
            unimplemented!()
        }

        async fn progress<'cx>(&'cx self) -> result::Result<(), ActionError> {
            unimplemented!()
        }

        async fn quarantine<'cx, 'a>(
            &'cx self,
            _: impl IntoCString + Send + 'a,
        ) -> result::Result<(), ActionError> {
            unimplemented!()
        }
    }

    #[derive(Debug, Default)]
    struct MockSmtpReply {
        error_reply: Option<(String, Option<String>, Vec<CString>)>,
    }

    impl MockSmtpReply {
        fn new() -> Self {
            Default::default()
        }
    }

    impl SetErrorReply for MockSmtpReply {
        fn set_error_reply<I, T>(
            &mut self,
            rcode: &str,
            xcode: Option<&str>,
            message: I,
        ) -> result::Result<(), SmtpReplyError>
        where
            I: IntoIterator<Item = T>,
            T: IntoCString,
        {
            self.error_reply = Some((
                rcode.into(),
                xcode.map(|c| c.into()),
                message.into_iter().map(|l| l.into_c_string()).collect(),
            ));
            Ok(())
        }
    }

    const ID: &str = "NONE";

    #[tokio::test]
    async fn client_send_writes_bytes() {
        let spamc = MockSpamc::new();

        let mut client = Client::new(spamc, "sender".into());
        client.send_header("name1", " value1").await.unwrap();
        client.send_header("name2", " value2\n\tcontinued").await.unwrap();
        client.send_eoh().await.unwrap();
        client.send_body_chunk(b"body").await.unwrap();

        assert_eq!(client.bytes_written(), 48);
        assert_eq!(
            as_mock_spamc(client.process.as_ref()).buf,
            b"name1: value1\r\nname2: value2\r\n\tcontinued\r\n\r\nbody".as_ref()
        );
    }

    #[tokio::test]
    async fn client_send_envelope_addresses() {
        let spamc = MockSpamc::new();

        let sender = "<sender@gluet.ch>";
        let recipient1 = "<recipient1@gluet.ch>";
        let recipient2 = "<recipient2@gluet.ch>";

        let mut client = Client::new(spamc, sender.into());
        client.add_recipient(recipient1.into());
        client.add_recipient(recipient2.into());

        client.send_envelope_sender().await.unwrap();
        client.send_envelope_recipients().await.unwrap();

        assert_eq!(
            as_mock_spamc(client.process.as_ref()).buf,
            format!(
                "X-Envelope-From: {sender}\r\n\
                 X-Envelope-To: {recipient1},\r\n\
                 \t{recipient2}\r\n",
            )
            .as_bytes()
        );
    }

    #[tokio::test]
    async fn client_process_invalid_response() {
        let spamc = MockSpamc::with_output(b"invalid message response".to_vec());

        let client = Client::new(spamc, "sender".into());

        let mut reply = MockSmtpReply::new();
        let actions = MockEomActions::new();
        let config = Default::default();

        let status = client.process(ID, &mut reply, &actions, &config).await.unwrap();

        assert_eq!(status, Status::Tempfail);
    }

    #[tokio::test]
    async fn client_process_reject_spam() {
        let spamc = MockSpamc::with_output(b"X-Spam-Flag: YES\r\n\r\n".to_vec());

        let client = Client::new(spamc, "sender".into());

        let mut reply = MockSmtpReply::new();
        let actions = MockEomActions::new();
        let config = Config::builder().reject_spam(true).build();

        let status = client.process(ID, &mut reply, &actions, &config).await.unwrap();

        assert_eq!(status, Status::Reject);
        assert_eq!(
            reply.error_reply,
            Some((
                "550".into(),
                Some("5.7.1".into()),
                vec![c_str!("Spam message refused").into()],
            )),
        );
    }

    #[tokio::test]
    async fn client_process_rewrite_spam() {
        let spamc = MockSpamc::with_output(
            b"X-Spam-Flag: YES\r\nX-Spam-Level: *****\r\n\r\nReport".to_vec(),
        );

        let mut client = Client::new(spamc, "sender".into());

        client.send_header("x-spam-level", " *").await.unwrap();
        client.send_header("x-spam-report", " ...").await.unwrap();

        let mut reply = MockSmtpReply::new();
        let actions = MockEomActions::new();
        let config = Default::default();

        let status = client.process(ID, &mut reply, &actions, &config).await.unwrap();

        assert_eq!(status, Status::Continue);

        let called = actions.called.lock().unwrap();
        assert_eq!(
            called.as_slice(),
            [
                Action::ChangeHeader(c_str!("X-Spam-Level").into(), 1, None),
                Action::InsertHeader(0, c_str!("X-Spam-Level").into(), c_str!(" *****").into()),
                Action::InsertHeader(0, c_str!("X-Spam-Flag").into(), c_str!(" YES").into()),
                Action::ChangeHeader(c_str!("x-spam-report").into(), 1, None),
                Action::ReplaceBody(b"Report".to_vec()),
            ]
        );
    }

    #[tokio::test]
    async fn client_process_skipped_body_not_replaced() {
        let spamc = MockSpamc::with_output(
            b"X-Spam-Flag: YES\r\nX-Spam-Level: *****\r\n\r\nReport".to_vec(),
        );

        let mut client = Client::new(spamc, "sender".into());
        client.skip_body();

        let mut reply = MockSmtpReply::new();
        let actions = MockEomActions::new();
        let config = Default::default();

        let status = client.process(ID, &mut reply, &actions, &config).await.unwrap();

        assert_eq!(status, Status::Continue);

        let called = actions.called.lock().unwrap();
        assert!(called.contains(&Action::InsertHeader(
            0,
            c_str!("X-Spam-Level").into(),
            c_str!(" *****").into()
        )));
        assert!(!called.contains(&Action::ReplaceBody(b"Report".to_vec())));
    }
}
